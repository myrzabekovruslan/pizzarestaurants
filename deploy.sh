#!/bin/sh

ssh -o StrictHostKeyChecking=no ruslan@193.187.96.241 << 'ENDSSH'
  cd /app
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker pull web
  docker pull nginx
  docker-compose -f docker-compose.prod.yml up -d
ENDSSH