from rest_framework import viewsets, permissions, generics, status, serializers, mixins
from .serializers import PizzaSerializer, PizzaThicknessSerializer, CheeseTypeSerializer, SecretIngredientSerializer
from .models import Pizza, PizzaThickness, CheeseType, SecretIngredient
from rest_framework.response import Response
from rest_framework.decorators import action
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


class PizzaViewSet(viewsets.ModelViewSet):
    """Вьюшки для пиццы"""
    queryset = Pizza.objects.all()
    serializer_class = PizzaSerializer


class PizzaThicknessViewSet(viewsets.ModelViewSet):
    """Вьюшки для толщины пиццы"""
    queryset = PizzaThickness.objects.all()
    serializer_class = PizzaThicknessSerializer


class CheeseTypeViewSet(viewsets.ModelViewSet):
    """Вьюшки для видов сыра"""
    queryset = CheeseType.objects.all()
    serializer_class = CheeseTypeSerializer


class SecretIngredientViewSet(viewsets.ModelViewSet):
    """Вьюшки для секретных ингридиентов"""
    queryset = SecretIngredient.objects.all()
    serializer_class = SecretIngredientSerializer