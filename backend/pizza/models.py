from django.db import models
from rest_framework.reverse import reverse
from restaurant.models import Restaurant


class CheeseType(models.Model):
    name = models.CharField(max_length=100, verbose_name='Вид сыра')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Виды сыра"
        verbose_name = "Сыр"
        db_table = "cheese_types"


class PizzaThickness(models.Model):
    name = models.CharField(max_length=50, verbose_name='Толщина пиццы')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Разновидность толщины пиццы"
        verbose_name = "Толщина"
        db_table = "pizza_thicknesses"


class SecretIngredient(models.Model):
    name = models.CharField(max_length=100, verbose_name='Секретный ингридиент')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Секретные ингридиенты"
        verbose_name = "Секретный ингридиент"
        db_table = "secret_ingredients"


class Pizza(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    cheese_type = models.ManyToManyField(
        CheeseType,
        blank=True,
        related_name='pizzas',
        verbose_name='Виды сыра'
    )
    thickness = models.ForeignKey(
        PizzaThickness,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='pizzas',
        verbose_name='Толщина пиццы'
    )
    secret_ingredients = models.ManyToManyField(
        SecretIngredient,
        blank=True,
        related_name='pizzas',
        verbose_name='Секретные ингридиенты'
    )
    restaurant = models.ManyToManyField(
        Restaurant,
        blank=True,
        related_name='pizzas',
        verbose_name='Ресторан',
    )

    def get_absolute_url(self):
        return reverse('pizza-detail', args=[str(self.id)])

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Пиццы"
        verbose_name = "Пицца"
        db_table = "pizzas"