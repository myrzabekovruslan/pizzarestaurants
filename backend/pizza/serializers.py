from rest_framework import serializers
from .models import PizzaThickness, Pizza, CheeseType, SecretIngredient


class PizzaSerializer(serializers.ModelSerializer):
    """Сериализатор для пицц"""

    class Meta:
        model = Pizza
        fields = "__all__"


class PizzaThicknessSerializer(serializers.ModelSerializer):
    """Сериализатор для толщины пиццы"""

    class Meta:
        model = PizzaThickness
        fields = "__all__"


class SecretIngredientSerializer(serializers.ModelSerializer):
    """Сериализатор для секретных ингредиентов"""

    class Meta:
        model = SecretIngredient
        fields = "__all__"


class CheeseTypeSerializer(serializers.ModelSerializer):
    """Сериализатор для видов сыра"""

    class Meta:
        model = CheeseType
        fields = "__all__"