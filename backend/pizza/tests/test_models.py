from rest_framework.test import APITestCase
from pizza.models import PizzaThickness, Pizza, CheeseType, SecretIngredient
from restaurant.models import Restaurant

class PizzaModelTests(APITestCase):

    def setUp(self):
        self.thickness = PizzaThickness.objects.create(name="Тонкая")
        self.cheese_type = CheeseType.objects.create(name="Черри")
        self.secret_ingredient = SecretIngredient.objects.create(name="Лесные грибы")
        self.restaurant = Restaurant.objects.create(name="LoLa Pizza")
        self.pizza = Pizza.objects.create(
            name="Pepperoni",
            thickness=self.thickness,
        )
        self.pizza.cheese_type.add(self.cheese_type)
        self.pizza.secret_ingredients.add(self.secret_ingredient)
        self.pizza.restaurant.add(self.restaurant)

    def test_pizza_object_name_is_name_field(self):
        pizza = self.pizza
        expected_object_name = "Pepperoni"
        self.assertEquals(expected_object_name, str(pizza))

    def test_pizza_get_absolute_url(self):
        pizza = self.pizza
        self.assertEquals(pizza.get_absolute_url(), f'/api/v1/pizza/{pizza.id}/')

    def test_cheese_type_object_name_is_name_field(self):
        cheese_type = self.cheese_type
        expected_object_name = "Черри"
        self.assertEquals(expected_object_name, str(cheese_type))

    def test_secret_ingredient_object_name_is_name_field(self):
        secret_ingredient = self.secret_ingredient
        expected_object_name = "Лесные грибы"
        self.assertEquals(expected_object_name, str(secret_ingredient))

    def test_thickness_object_name_is_name_field(self):
        thickness = self.thickness
        expected_object_name = "Тонкая"
        self.assertEquals(expected_object_name, str(thickness))