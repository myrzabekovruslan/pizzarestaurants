from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase
from pizza.serializers import PizzaSerializer, PizzaThicknessSerializer, CheeseTypeSerializer, \
                              SecretIngredientSerializer
from restaurant.serializers import RestaurantSerializer
from pizza.models import PizzaThickness, Pizza, CheeseType, SecretIngredient
from restaurant.models import Restaurant

class PizzaViewTests(APITestCase):

    def setUp(self):
        self.thickness = PizzaThickness.objects.create(name="Тонкая")
        self.cheese_type_1 = CheeseType.objects.create(name="Чеддер")
        self.cheese_type_2 = CheeseType.objects.create(name="Моцарелла")
        self.secret_ingredient = SecretIngredient.objects.create(name="Лесные грибы")
        self.restaurant = Restaurant.objects.create(name="LoLa Pizza")
        self.pizza = Pizza.objects.create(
            name="Pepperoni",
            thickness=self.thickness,
        )
        self.pizza.cheese_type.add(self.cheese_type_1, self.cheese_type_2)
        self.pizza.secret_ingredients.add(self.secret_ingredient)
        self.pizza.restaurant.add(self.restaurant)


    def test_list(self):
        response = self.client.get(reverse('pizza-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)

    def test_retrieve(self):
        response = self.client.get(reverse('pizza-detail', kwargs={'pk': self.pizza.id}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), PizzaSerializer(self.pizza).data)

    def test_create(self):
        response = self.client.post(reverse('pizza-list'), data={
            'name': "Mexico",
            'cheese_type': [self.cheese_type_1.id, self.cheese_type_2.id],
            'thickness': self.thickness.id,
            'secret_ingredients': self.secret_ingredient.id,
            'restaurant': self.restaurant.id
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        created_pizza = Pizza.objects.get(name='Mexico')
        self.assertEqual(response.json(), PizzaSerializer(created_pizza).data)

    def test_update(self):
        response = self.client.put(reverse('pizza-detail', kwargs={'pk': self.pizza.id}), data={
            'name': 'Four cheese',
            'cheese_type': self.cheese_type_1.id,
            'thickness': self.thickness.id,
            'secret_ingredients': self.secret_ingredient.id,
            'restaurant': self.restaurant.id
        })
        updated_pizza = Pizza.objects.get(pk=self.pizza.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), PizzaSerializer(updated_pizza).data)

    def test_partial_update(self):
        response = self.client.patch(reverse('pizza-detail', kwargs={'pk': self.pizza.id}), data={
            'name': 'Four cheese',
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), {
            'id': self.pizza.id,
            'name': 'Four cheese',
            'cheese_type': [self.cheese_type_1.id, self.cheese_type_2.id],
            'thickness': self.thickness.id,
            'secret_ingredients': [self.secret_ingredient.id],
            'restaurant': [self.restaurant.id]
        })

    def test_delete(self):
        response = self.client.delete(reverse('pizza-detail', kwargs={'pk': self.pizza.id}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        deleted_status = False
        try:
            pizza = Pizza.objects.get(pk=self.pizza.id)
        except Pizza.DoesNotExist:
            deleted_status = True
        self.assertEqual(deleted_status, True)