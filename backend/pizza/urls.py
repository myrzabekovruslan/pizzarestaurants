from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import PizzaViewSet, PizzaThicknessViewSet, CheeseTypeViewSet, SecretIngredientViewSet


router = DefaultRouter()
router.register('pizza', PizzaViewSet, basename='pizza')
router.register('pizza_thickness', PizzaThicknessViewSet, basename='pizza_thickness')
router.register('cheese_type', CheeseTypeViewSet, basename='cheese_type')
router.register('secret_ingredient', SecretIngredientViewSet, basename='secret_ingredient')


urlpatterns = [
    path("", include(router.urls)),

]