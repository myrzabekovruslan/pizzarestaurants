from django.contrib import admin
from .models import Pizza, PizzaThickness, CheeseType, SecretIngredient

admin.site.register(Pizza)
admin.site.register(PizzaThickness)
admin.site.register(CheeseType)
admin.site.register(SecretIngredient)