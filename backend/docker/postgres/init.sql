CREATE USER postgres with PASSWORD 'admin';

CREATE DATABASE bio_test;
GRANT ALL PRIVILEGES ON DATABASE bio_test TO postgres;