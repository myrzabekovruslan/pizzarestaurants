from rest_framework import viewsets
from .serializers import RestaurantSerializer
from .models import Restaurant


class RestaurantViewSet(viewsets.ModelViewSet):
    """Вьюшки для ресторанов"""
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer