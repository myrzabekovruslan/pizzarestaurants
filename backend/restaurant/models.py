from django.db import models
from rest_framework.reverse import reverse


class Restaurant(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название ресторана')
    street = models.CharField(max_length=100,
                              null=True,
                              blank=True,
                              verbose_name='Улица')
    house = models.CharField(max_length=10,
                             null=True,
                             blank=True,
                             verbose_name='Номер здания')

    def get_absolute_url(self):
        return reverse('restaurant-detail', args=[str(self.id)])

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Рестораны"
        verbose_name = "Ресторан"
        db_table = "restautants"