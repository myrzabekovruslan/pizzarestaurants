from rest_framework.test import APITestCase
from restaurant.models import Restaurant

class RestaurantModelTests(APITestCase):

    def setUp(self):
        self.test_restaurant = Restaurant.objects.create(name="MamaSita")

    def test_object_name_is_name_field(self):
        restaurant = self.test_restaurant
        expected_object_name = "MamaSita"
        self.assertEquals(expected_object_name, str(restaurant))

    def test_get_absolute_url(self):
        restaurant = self.test_restaurant
        self.assertEquals(restaurant.get_absolute_url(), f'/api/v1/restaurant/{restaurant.id}/')