from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase
from restaurant.models import Restaurant
from restaurant.serializers import RestaurantSerializer


class RestaurantViewTests(APITestCase):

    def setUp(self):
        self.restaurant = Restaurant.objects.create(
            name="MamaSita",
            street="Орынбор",
            house="32A"
        )

    def test_list(self):
        response = self.client.get(reverse('restaurant-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)

    def test_retrieve(self):
        response = self.client.get(reverse('restaurant-detail', kwargs={'pk': self.restaurant.id}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), RestaurantSerializer(self.restaurant).data)

    def test_create(self):
        response = self.client.post(reverse('restaurant-list'), data={
            'name': 'DoDo Pizza',
            'street': 'Кабанбай батыра',
            'house': '81',
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        created_restaurant = Restaurant.objects.get(name='DoDo Pizza')
        self.assertEqual(response.json(), RestaurantSerializer(created_restaurant).data)

    def test_update(self):
        response = self.client.put(reverse('restaurant-detail', kwargs={'pk': self.restaurant.id}), data={
            'name': 'Papas',
            'street': 'Туркестан',
            'house': '12',
        })
        updated_restaurant = Restaurant.objects.get(pk=self.restaurant.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), RestaurantSerializer(updated_restaurant).data)

    def test_partial_update(self):
        response = self.client.patch(reverse('restaurant-detail', kwargs={'pk': self.restaurant.id}), data={
            'name': 'Papas',
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), {
            'id': self.restaurant.id,
            'name': 'Papas',
            'street': 'Орынбор',
            'house': '32A'
        })

    def test_delete(self):
        response = self.client.delete(reverse('restaurant-detail', kwargs={'pk': self.restaurant.id}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        deleted_status = False
        try:
            restaurant = Restaurant.objects.get(pk=self.restaurant.id)
        except Restaurant.DoesNotExist:
            deleted_status = True
        self.assertEqual(deleted_status, True)