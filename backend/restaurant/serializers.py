from rest_framework import serializers
from .models import Restaurant


class RestaurantSerializer(serializers.ModelSerializer):
    """Сериализатор для рестаранов"""

    class Meta:
        model = Restaurant
        fields = "__all__"
